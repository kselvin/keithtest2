//
//  ViewController.swift
//  AFSample
//
//  Created by Keith Selvin on 12/29/17.
//  Copyright © 2017 Keith Selvin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    var testVariable: String? 

    override func viewDidLoad() {
        super.viewDidLoad()
        getCryptoPrice()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getCryptoPrice(){
        
        Alamofire.request("https://api.coindesk.com/v1/bpi/currentprice.json", method: .get).responseJSON { (response) in
            if let JSON = response.result.value as? [String: Any] {
                if let bitcoinObject = JSON["bpi"] as? [String: Any] {
                    print(bitcoinObject["USD"] ?? "can't find it")
                }
            }
            
            if let value = response.result.value {
                let json = JSON(value)
                print(json["bpi"]["USD"])
            }
            
            
        }
        
        
    }

    
    func withSwifyJSON(){
        Alamofire.request("https://codewithchris.com/code/afsample.json", method: .get).responseJSON { (response) in
            //check if result has a value
            
            //MARK - Without Using SwiftyJSON
            if let JSON = response.result.value as? [String: Any]{
                if let firstKey = JSON["firstkey"] {
                    print(firstKey)
                }
                if let secondkey = JSON["secondkey"] as? NSArray {
                    print(secondkey[0])
                    print(secondkey[1])
                }
            }
            
            //MARK - Using SwiftyJSON
            if let value = response.value {
                let json = JSON(value)
                print(json["firstkey"].stringValue)
                print(json["secondkey"].arrayValue)
                print(json["secondkey"][0].stringValue)
            }
        }
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }

}

